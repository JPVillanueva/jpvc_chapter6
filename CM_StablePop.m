clc

flocknumber=1000;
%number of flocks that need to be sampled


eval(params_file)
%Call the parameters declared in systemvc



Cpopdist =(csvread('Cpopdist.csv',1));
Chalpha=(csvread('Chalpha.csv',1));
mintrading=(csvread('LHSmintradingEggAdjust.csv',1));
lhsfeed=(csvread('lhsfeed.csv',1));


transmcoeff=transmcoeff;
%model extension not used in chapter 6

infectend=zeros(size(transmcoeff));
%model extension not used in chapter 6

totvisityear=zeros(size(Cpopdist,1),1); 
%matrix that stores totyear visist of traders


% rowsellingmatrix=1:floor((maxtime+1)/7);S(end,1:5)'S(end,1:5)'S(end,1:5)'
% chwkharv=zeros(size(rowsellingmatrix,2),size(Cpopdist,1));
% chwkharv(1,:)=Cpopdist(:,1);
%Commented. Records all week harvesting

chwkharv=zeros(2,size(Cpopdist,1));
chwkharv(1,:)=Cpopdist(:,1);
%Records harvesting of chickens in the last week as a
%sample of what is usually harvested (steady-state dynamics)

eggwkharv=zeros(2,size(Cpopdist,1));
eggwkharv(1,:)=Cpopdist(:,1);
%Records harvesting of eggs in the last week as a
%sample of what is usually harvested (steady-state dynamics)


totwkimp=zeros(2,size(Cpopdist,1));
totwkimp(1,:)=Cpopdist(:,1);
%Records importation of birds in the last week as a
%sample of what is usually imported (steady-state dynamics)

wkharv=zeros(9,size(Cpopdist,1));
wkharv(1,:)=Cpopdist(:,1);
%Matrix
%row1=flock size
%row2= chicken harvested per week
%row3= egg harvested per week
%row4= tot importations per week
%row5to9= number of chicks, egrower, lgrower, hens and rooster at the end


for g= 1:size(transmcoeff,2) %Extension not used in chapter 6
    
for a=1:size(Cpopdist,1)
    
%INITIAL POPULATION (from Villanueva-Cabezas 2016)

Ninitial=Cpopdist(a,2);
%population at equilibrium

So=(Cpopdist(a,3:7))*Ninitial;
Eo=Eo;
Io=Io;


Soldo=Soldo;
eggo=[Cpopdist(a,8)];
seggo=seggo;
extrahatcho=extrahatcho;


%POPULATION PARAMETERS

eggmort=eggmort;
agemort=agemort;
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]

params.EggStage=EggStage;
params.AgeStages=AgeStages;
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]


params.durationES=diff(params.EggStage)*(week/tstep);
params.durationAS=diff(params.AgeStages)*(week/tstep);
%in days; in this step I transform the duration of age stage to days, 
%so it later can be modified using durationAS/tstep 



expdecay=@(agemort,durationAS,tstep) -(log(1-agemort))/(durationAS/tstep);
%Survival age-stage= 1-agemort; then, survival=e^(-rate*durationAS/tstep))
%This anonymous function estimates death rate according to tstep,
%assuming exponential decay. Here I give the inputs
%(agemort,durationAS,tstep),and to the right I define the function itself.

eggsdeath= expdecay(eggmort(1),params.durationES,tstep);

chicksdeath= expdecay(agemort(1),params.durationAS(1),tstep);
Egrowdeath= expdecay(agemort(2),params.durationAS(2),tstep);
Lgrowdeath= expdecay(agemort(3),params.durationAS(3),tstep);
Adfemdeath= expdecay(agemort(4),params.durationAS(4),tstep);
Admaledeath= expdecay(agemort(5),params.durationAS(5),tstep);



params.eggdelta=[eggsdeath];
params.delta=[chicksdeath Egrowdeath Lgrowdeath Adfemdeath Admaledeath];
%These vector pass death rates to function
%[egg]
%[chicks, Egrow, Lgrow, Afem, Amale]   



params.h=[0 0 Chalpha(a,2:4)];
%h is the proportion of birds available for harvesting;


params.alfa=[0 0 Chalpha(a,5:7)];
%alfa represents the rate at which h is harvested;


% HENS FERTILITY

params.Eggs=Eggs; 
%Eggs/hen/year/tstep in days


params.P= P; 
%usable eggs

params.w= w; 
%eggs that go to incubation

params.br=br;
%chickens that can be broody

%FLAGS (These regulate some of the ODEs; affect only birds)

params.hatching=hatching;
%flag to allow hatching only in the chicks compartment

params.allowageing=allowageing;
%flag to only allow ageing of chicks, Egrow, Lgrow


params.ageingprop=xratio;
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else.

params.y=y;
%proportion of importation that is day-old chicks.

%DISEASE PARAMETERS (extension not used in Chapter 6)
params.beta=transmcoeff(g)*ones(size(So,2),size(So,2));
params.sigma=sigmaparameter;
params.gamma=gammaparameter;



%call function
[T,S,E,I,Sold,egg,sellegg,totimport,extrahatch]= CM(params,So,Eo,Io,Soldo,eggo,seggo,totimporto,extrahatcho,maxtime,tstep);



totpopulation=sum(S(end,1:5));
norm_prop=(S(end,1:5)/totpopulation);


%Results


newtime=1:floor(length(T)/7);


%CHIKENS FOR SELLING/WEEK
totalharvest=diff(sum(Sold,2));
weekharvestchicken=reshape(totalharvest(1:(7*length(newtime))),7,length(newtime))';
weeksumchicken=sum(weekharvestchicken,2);
chwkharv(2,a)=weeksumchicken(end);


% %EGGS FOR SELLING/WEEK
totalharvestegg=diff(sellegg);
weekharvestegg=reshape(totalharvestegg(1:(7*length(newtime))),7,length(newtime))';
weeksumegg=sum(weekharvestegg,2);
eggwkharv(2,a)=weeksumegg(end); 


% %CHIKENS IMPORTED PER WEEK
totaltotimport=diff(totimport);
weektotimportbought=reshape(totaltotimport(1:(7*length(newtime))),7,length(newtime))';
importweek=sum(weektotimportbought,2);
totwkimp(2,a)=importweek(end);


wkharv(2,a)=weeksumchicken(end);
wkharv(3,a)=weeksumegg(end); 
wkharv(4,a)=importweek(end);
wkharv(5:9,a)=S(end,1:5)';



end
end



%%
%Simulation of trading per village



rep=size(mintrading,1);
% number of villages simulated (10000 in chapter 6)

villagesize=zeros(flocknumber,rep);
totchwk=zeros(size(nofw,2),rep);
toteggwk=zeros(size(nofw,2),rep);
totimpwk=zeros(size(nofw,2),rep);

totfeed=zeros(1,rep);

rng('default');

for r= 1:rep
rng(r); %for repeatibility
randomvillage=datasample(wkharv,rep,2);
%Here I make a random sample to simulate a village. 

villagesize(:,r)=randomvillage(1,:)';

%function[ladif,totwk]=lala(wkharv,nofw,propselling,mintrade)
totchwk(:,r)=lala(randomvillage(2,:),nofw,propsellchicken,mintrading(r,1));

toteggwk(:,r)=lala(randomvillage(3,:),nofw,propsellegg,mintrading(r,2));

totimpwk(:,r)=lala(randomvillage(4,:),nofw,propimport,mintrading(r,3));

totfeed(:,r)=feed(randomvillage(5:9,:),propfeeding,lhsfeed(r,1:5),lhsfeed(r,6));
end
%Presumed number of visits derived from these activities.
%It is acknowledged that not all birds or eggs available are sold in a given week.
%Also, a minimum number of units is needed for a trading visit.
%Minimum based on Diwayanto (1999), Wiraksudakul (2014)and the
%proportion of producers trading or not in each cluster. 

%here I am saving a random village (1000 flocks) to sample from here for my
%scenarios.
% Table=table(randomvillage);
 %filename='randomvillage.csv';
 %writetable(Table,filename)

%%

% subplot(4,2,1)
% sellchicken = [totchwk(:,1), totchwk(:,2), totchwk(:,3), totchwk(:,4), totchwk(:,5), totchwk(:,6),...
%     totchwk(:,7),totchwk(:,8),totchwk(:,9),totchwk(:,10)];
% boxplot(sellchicken)
% 
% 
% subplot(4,2,2)
% sellegg= [toteggwk(:,1), toteggwk(:,2), toteggwk(:,3), toteggwk(:,4), toteggwk(:,5), toteggwk(:,6),...
%     toteggwk(:,7),toteggwk(:,8),toteggwk(:,9),toteggwk(:,10)];
% boxplot(sellegg)
% 
% 
% subplot(4,2,3)
% importchicken= [totimpwk(:,1), totimpwk(:,2),totimpwk(:,3), totimpwk(:,4), totimpwk(:,5), totimpwk(:,6),...
%     totimpwk(:,7),totimpwk(:,8),totimpwk(:,9),totimpwk(:,10)];
% boxplot(importchicken)
% 
% subplot(4,2,4)
% meanwksellchk=(sum(sellchicken,1)/52)';
% meanwksellegg=(sum(sellegg,1)/52)';
% meanwkimport=(sum(importchicken,1)/52)';
% 
% meanwkvisits=[meanwksellchk meanwksellegg meanwkimport];
% 
% boxplot(meanwkvisits)
% %%
% mwkchsell=(sum(totchwk,1)/52)';
% mwkeggsell=(sum(toteggwk,1)/52)';
% mwkimport=(sum(totimpwk,1)/52)';
% mwkfeed=(totfeed)';
% 
% mwktotvisit=round([mwkchsell mwkeggsell mwkimport mwkfeed]);
% boxplot(mwktotvisit)
% 
%  Table=table(mwktotvisit);
%  filename='Trips.csv';
%  writetable(Table,filename)
% %  
 
%%
%This is to estimate the size of the villages that are dominated by any of the specialist clusters
 
% tablesize=table(sum(villagesize,1));
% filename='villagesizes.csv';
% writetable(tablesize,filename);







