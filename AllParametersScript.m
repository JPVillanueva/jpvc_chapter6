%NON-SPECIALIST PARAMETERS FOR DETERMINISTIC MODEL 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TIME

maxtime=3000;
%Simultion time
tstep=1;
%in days
week=7;
%in days
year=365;
%in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POPULATION SIZE AND EMPIRICAL DISTRIBUTION


NSfitflocks=(csvread('NSflocksize.csv',1));
%flock sizes sampled from data fited with negative binomial distribution


CMpopulationsize=(NSfitflocks(:,2));


indices = find(CMpopulationsize >= 500);
CMpopulationsize(indices) = [];
%Remove observations larger than 500 individuals which are not
%representative of village chicken production (Muladno&Thieme, 2009).

CMempirical_proportions = [0.583, 0, 0, 0.25, 0.167];
%[chicks egrower lgrower afem amale]


EggStage=[-3 0];
chdur=4;
egdur=10;
lgdur=10;
hedur=80;
rsdur=28;
%duration each age stage in weeks


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FMINCON

dummytranscoeff=0;
%only used in fmincon process

repetitionfmincon=4;
%loops FMINCON. This recycles the values calulated by Fmincon and uses it
%as input for optimal solutions.

x0 = [1 1 1  1/(lgdur*week) 1/(hedur*week) 1/(rsdur*week)];
%initial guess for fmincon

lb = [1 1 1  1/(lgdur*week*2) 1/(hedur*week*2) 1/(rsdur*week*2)];
% the slowest the chickens leave the system. alpha in days
up = [1 1 1 .99 .99 .99];
% the fastest the chickens leave the system. alpha in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INITIAL POPULATION

%S derives from CM_BestH_Alpha
Eo=[0 0 0 0 0];
Io=[0 0 0 0 0];
%Eo and Io model extensions not used in chapter 6.
Soldo=[0 0 0 0 0];
%this represents harvesting at time zero
eggo=[0];
%eggs for incubation at time zero
seggo=[0];
% eggs for selling at time zero
totimporto=[0];
%bird imported per timestep
extrahatcho=[0];
%chicks that hatch but are not considerded in the population

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AGE-STAGE DURATION AND MORTALITY 

eggmort=[0.53];
agemort=[0.23 0.19 0.19 0.05 0.018];
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]


AgeStages=[0,chdur,(chdur+egdur),(chdur+egdur+lgdur),...
    (chdur+egdur+lgdur+hedur),(chdur+egdur+lgdur+hedur+rsdur)]';
% AgeStages=[0 4 14 24 52 80]';
%in weeks; this shows the week that each age-stage finishes
%[egg]
%[chicks egrower lgrower afem amale]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HENS FERTILITY


negg=10.8;
%eggs/clutch/hen
q=2;
%clutch/hen/year
br=1;
%proportion chickens that can be broody
egghenyear=negg*q;
%eggs hen year in clutches
Eggs=egghenyear/(year/tstep); 
%eggs per hen per timestep
%Currently in days -> JPVillanueva 27.9.17
P=0.9; 
%usable eggs
w=(1/2.6); 
%rate of eggs available for selling (in days). Based on the total number of eggs laid
%per hen in a year less the eggs that go into clutches


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FLAGS AND AGEING RATIO (These regulate some of the ODEs; affect only birds)

hatching=[1 0 0 0 0];
%flag to allow hatching only in the chicks compartment
allowageing=[1 1 1 0 0];
%flag to only allow ageing of chicks, Egrow, Lgrow
xratio=[1 1 1 0.6 0.4];
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else. Important to allow ageing according to rate
%hen:rooster observed(4th and 5th position)
y=1;
%Percentage of importation that are day-old chicks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DISEASE PARAMETERS (not in use in Chapter 6)

transmcoeff=[0];
%beta parameters
sigmaparameter= 0;
gammaparameter= 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ESTIMATION OF HARVESTING PER WEEK
nofw=[1:1:52];
%One year simulation and estimation per week.

propsellchicken=0.333;
propsellegg=0.01; %assuming some may sell some eggs sometimes
propimport=0.413;
propfeeding=0.49;
%these are the respective proportions that import/sell beyond the village



