clc

eval(params_file)
%Call the parameters declared in systemvc


CMhalpha=zeros(length(CMpopulationsize),7);
CMhalpha(:,1)=CMpopulationsize;
%Matrix that stores all best values for h and alpha

%FMINCON routine

 for m=1:length(CMpopulationsize)

     
sum_of_squares =@(x) sum((x - CMempirical_proportions*CMpopulationsize(m)) .^ 2);

error_fun =@(th) sum_of_squares(CM_steady_state(params_file,maxtime,tstep,CMempirical_proportions,CMpopulationsize(m),m,th));


% in days
x0 = x0;
A = zeros(length(x0));
b = zeros(length(x0), 1);
Aeq = zeros(length(x0));
beq = zeros(length(x0), 1);
lb = lb;
up = up; 



repetitionfmincon=repetitionfmincon;
%loops FMINCON. This recycles the values calulated  and uses it as input in
%next loop.

for w= 1:repetitionfmincon

options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
[x, fval, exitflag, output] = fmincon(error_fun,x0, A, b, Aeq, beq, lb, up,[],options);

x


x0=x;
end


x

fval

if exitflag < 1
    'bad'
else
    'good'
end

CMhalpha(m,2:7)=x;
 end
 
 
 Table=table(CMhalpha(:,1),CMhalpha(:,2),CMhalpha(:,3),CMhalpha(:,4),CMhalpha(:,5),CMhalpha(:,6),CMhalpha(:,7));
 Table.Properties.VariableNames = {'popsize' 'Lgrowh' 'AFemh' 'AMaleh' 'Lgrowalpha' 'AFemalpha' 'AMalealpha'}
 filename='Chalpha.csv';
 writetable(Table,filename)
 
 