
function [visitsperwk] = systemvc(systemtype)

%Switch to simulate different types of village chicken production system

switch systemtype 
    case 1
       params_file= 'AllParametersScript';
    %Non-specialist

    case 2
       params_file= 'AllParametersBIRDSp';
     %Bird-specialist 
        
    case 3
        
        params_file= 'AllParametersEGGSp';
     %Egg-specialist
        
end

       CM_BestH_Alpha
       CM_Script
       CM_StablePop



end

