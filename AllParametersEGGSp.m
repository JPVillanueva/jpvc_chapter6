%EGG-SPECIALIST PARAMETERS FOR DETERMINISTIC MODEL 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%TIME

maxtime=3000;
%Simultion time
tstep=1;
%in days
week=7;
%in days
year=365;
%in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POPULATION SIZE AND EMPIRICAL DISTRIBUTION


ESfitflocks=(csvread('ESflocksize.csv',1));
%flock sizes sampled from data fited with negative binomial distribution


CMpopulationsize=(ESfitflocks(:,2));


indices = find(CMpopulationsize >= 500);
CMpopulationsize(indices) = [];
%Remove observations larger than 500 individuals which are not
%representative of village chicken production (Muladno&Thieme, 2009).



CMempirical_proportions = [0.467, 0, 0, 0.333, 0.20];
%[chicks egrower lgrower afem amale]


EggStage=[-3 0];
chdur=4;
egdur=10;
lgdur=10;
hedur=80;
rsdur=28;
%duration each age stage in weeks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FMINCON

dummytranscoeff=0;
%only used in fmincon process

repetitionfmincon=4;
%loops FMINCON. This recycles the values calulated by Fmincon and uses it
%as input for optimal solutions.

x0 = [1 1 1 1/(lgdur*week) 1/(hedur*week) 1/(rsdur*week)];
%initial guess for fmincon.

lb = [1 1 1 1/(lgdur*week*2) 1/(hedur*week*2) 1/(rsdur*week*2)];
%the slowest chickens leave the system. alpha in days

up = [1 1 1 .99  .99 .99];
% the fastest the chickens leave the system. alpha in days
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%INITIAL POPULATION

%S derives from CM_BestH_Alpha
Eo=[0 0 0 0 0];
Io=[0 0 0 0 0];
%Eo and Io model extensions not used in chapter 6.
Soldo=[0 0 0 0 0];

%this represents harvesting at time zero
eggo=[0];
%eggs for incubation at time zero
seggo=[0];
% eggs for selling at time zero
totimporto=[0];
%bird imported per timestep
extrahatcho=[0];
%chicks that hatch but are not considerded in the population

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AGE-STAGE DURATION AND MORTALITY 

eggmort=[0.53];
agemort=[0.15 0.13 0.13 0.05 0.018];
% AgeStages=[0 4 14 24 52 80]';
%Age-stage mortality (from literature)
%[egg]
%[chicks egrower lgrower afem amale]
%rs mortality in relation to 5% mortality in hens but rooster live less time

AgeStages=[0,chdur,(chdur+egdur),(chdur+egdur+lgdur),...
    (chdur+egdur+lgdur+hedur),(chdur+egdur+lgdur+hedur+rsdur)]';
%in weeks; this shows the week that each age-stage starts
%[egg]
%[chicks egrower lgrower afem amale]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HENS FERTILITY


negg=10.2;
%eggs/clutch/hen
q=2;
%clutch/hen/year
br=0.1;
%proportion of chickens that can be broody
egghenyear=negg*q;
%eggs hen year in clutches
Eggs=egghenyear/(year/tstep); 
%eggs per hen per timestep in days
P=0.9; 
%usable eggs
w=1/2.6; 
%rate of eggs available for selling (in days). Based on the total number of eggs laid
%per hen in a year less the eggs that go into clutches


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FLAGS AND AGEING RATIO (These regulate some of the ODEs; affect only birds)

hatching=[1 0 0 0 0];
%flag to allow hatching only in the chicks compartment
allowageing=[1 1 1 0 0];
%flag to only allow ageing of chicks, Egrow, Lgrow
xratio=[1 1 1 0.62 0.38];
%flag to indicate which proportion of the cohort will age into next
%age-stage. There is no cohort aging into chicks, only hatching that is
%added somewhere else. Important to allow ageing according to rate
%hen:rooster observed (4th and 5th position)
y=[0.8];
%Percentage ofimportation that are day-old chicks. These egg-producers
%import adult hens to keep production
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DISEASE PARAMETERS (not in use in Chapter 6)

transmcoeff=[0];
%beta parameters
sigmaparameter= 0;
gammaparameter= 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ESTIMATION OF HARVESTING PER WEEK

nofw=[1:1:52];
%One year simulation and estimation per week.

propsellchicken=0.875;
propsellegg=1;
propimport=0.438;
propfeeding=0.63;
%these are the respective proportions that import/sell beyond the village

